# TestReplicatedALS
It is a showcase project for [Replicated-ALS](https://gitlab.com/eyupalemdar/Replicated-ALS) plugin.

![Image](https://gitlab.com/eyupalemdar/Replicated-ALS/-/raw/main/Resources/Readme_Content_2.gif)

## Setting Up The Project
- Clone the repository recursively to download with submodule 'ReplicatedALS':  
  `git clone --recursive https://gitlab.com/eyupalemdar/TestReplicatedALS.git`
- Regenerate Visual Studio project files and build your project.
